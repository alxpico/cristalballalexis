package android.rico.crystalball;

public class Predictions       {

    private static Predictions predictions;
    private String[] answers;

    private Predictions() {
        answers = new String[] {
                "Your wish will come true.",
                "Your wish will not come true."
        };
    }

    public static Predictions get(){
        if(predictions == null) {
            predictions = new Predictions();
        }
        return predictions;
    }

    public String getPrediction() {
        return answers[0];
    }


}
